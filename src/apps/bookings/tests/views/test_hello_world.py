from mock import Mock

from apps.bookings.views import hello_world


def test_returns_hello_world():
    request = Mock()
    response = hello_world(request)
    assert response
